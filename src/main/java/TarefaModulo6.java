public class TarefaModulo6 {

    public static void main(String[] args) {
        String[] nomesDosAlunos = new String[5];
        String[][] chamada = new String[5][6];

        for (int i = 0; i<5; i++) {
            nomesDosAlunos[i] = "Aluno "+ (i+1);
        }

        for (int i = 0; i<5; i++) {
            for (int j = 0; j<6; j++) {
                if (j==0) {
                    chamada[i][j] = nomesDosAlunos[i];
                }
                else {
                    chamada[i][j] = "V";
                }
            }
        }

        for (int i = 0; i<5; i++) {
            for (int j = 0; j < 6; j++) {
                System.out.print(chamada[i][j] + " ");
            }
            System.out.println("");
        }
    }
}
